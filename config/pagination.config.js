const paginationDefault = {
  routes: {
    include: [],  // Emptying include list will disable pagination
  },
  query: {
    limit: {
      name: 'per_page',
      default: 12
    }
  }
};

module.exports = paginationDefault;