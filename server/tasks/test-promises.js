const request = require('request')

module.exports = (data, done) => {
  const {name, method} = data,
    time = Date.now()

    //Attempt to fetch method file
    try {
      require('./' + method);
    } catch(e) {
      throw e; //Again, we should change this soon.
    }

    //Require method
    const promise = require('./' + method);

  promise()
    .then(done) //Nothing happens
    .catch((error)=>{

      //Send telegram message
      //sendMessage method
      const URL = `https://api.telegram.org/bot${process.env.TELEGRAM_TOKEN}/sendMessage`

      const options = {
        url: URL,
        method: 'POST',
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        json: {
          chat_id: process.env.TELEGRAM_CHANNEL_ID,
          text: `**Error!**\nMethod Failed: ${name}\nTimestamp: ${new Date(time)}\nNext Scheduled Run:${new Date(time + 15000)}`,
          parse_mode: 'markdown'
        }

      }

      request(options, (error, res, body) => {
        if (error) throw error; //for now
        done()
      })
    })
}
