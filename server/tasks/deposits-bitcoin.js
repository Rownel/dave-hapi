'use strict'
const Bitcore = require('bitcore-lib')
const Unirest = require('unirest')
const Async = require('async')
const lodash = require('lodash')
const Socket = require('../api/socket')
const polo = require('../lib/polo.js')
const Config = require('../../config.js')

exports.register = function (server, options, next) {
  const DepositAddress = server.plugins['hapi-mongo-models'].DepositAddress
  const Wallet = server.plugins['hapi-mongo-models'].Wallet
  const Transaction = server.plugins['hapi-mongo-models'].Transaction
  const User = server.plugins['hapi-mongo-models'].User
  const mailer = server.plugins.mailer
  const io = server.plugins['hapi-io'].io

  const sendBitcoinTx = function (depositWallet, fromAddress, amountToSend, toAddress, changeAddress, callback) {
    var newAmountToSend = amountToSend - parseInt(process.env.BTC_FEE,10)
    if(newAmountToSend < 5460) {
      return callback({err: 'amount is too small to send'})
    }
    var args = {
      fromAddress: fromAddress,
      amountToSend: newAmountToSend,
      toAddress: toAddress,
      changeAddress: changeAddress,
      callback: callback,
      txfee: parseInt(process.env.BTC_FEE,10)
    }
    // console.log("Deposited To: "+ depositWallet.address, "Send To:",toAddress,"Change:",changeAddress,"amount:",args.amountToSend,"fee:", args.txfee);

    var options = 'https://blockexplorer.com/api/addr/' + fromAddress + '/utxo'

    Unirest.get(options)
    .send()
    .end(function (response) {
      var resbody = response.body
      var utxos = []
      var availableAmount = 0

      for (var i = 0; i < response.body.length; i++) {
        var thistx = response.body[i]
        var txout = {
          'txId': thistx.txid,
          'vout': thistx.vout,
          'address': thistx.address,
          'scriptPubKey': thistx.scriptPubKey,
          'amount': thistx.amount
        }
        if (thistx.confirmations && thistx.confirmations > 0) {
          availableAmount += Bitcore.Unit.fromBTC(thistx.amount).toSatoshis()
          utxos.push(new Bitcore.Transaction.UnspentOutput(txout))
          // console.log("address of this output", txout.address)
        }
      }

      if (availableAmount < args.amountToSend + args.txfee) {
        return callback({err: 'insufficient funds to send TX'})// self.view("app", {secret:secret,err: "insufficient funds"});
      }

      try {
        var toAddress = new Bitcore.Address(args.toAddress)
        var hdPrivateKey = new Bitcore.HDPrivateKey.fromSeed(process.env.SEEDHEX)
        var hard = hdPrivateKey.derive("m/0'/0", Bitcore.Networks.livenet)
        var xpubPRIVKEY = hard.derive('m/' + depositWallet.height)
        var privateKey = new Bitcore.PrivateKey(xpubPRIVKEY.privateKey.toString())
        var changeAddress = new Bitcore.Address(args.changeAddress)
        var transaction = new Bitcore.Transaction()
          .from(utxos)          // Feed information about what unspent outputs one can use
          .to(toAddress, args.amountToSend)  // Add an output with the given amount of satoshis
          .change(args.changeAddress)
          .fee(args.txfee)      // Sets up a change address where the rest of the funds will go
          .sign(privateKey)// Signs all the inputs it can

        var hash = transaction.toObject().hash
        var serialized = transaction.serialize()
        // console.log(serialized);
        Unirest.post('http://btc.blockr.io/api/v1/tx/push')
        .send({hex: serialized})
        .end(function (response) {
          // console.log(response.body,"btcblockr");
          Unirest.post('https://api.coinprism.com/v1/sendrawtransaction')
          .send(serialized)
          .end(function (response) {
            // console.log(response.body);
            Unirest.post('https://blockchain.info/pushtx')
            .send({tx: serialized})
            .end(function (response) {
              // console.log(response.body);

            })
          })
        })
        return callback(transaction.toObject())
      } catch (e) {
        console.log(e)
        return callback({err: e})
      }
    })
  }

  const checkBitcoinTxs = function (depositWallets, callback) {
    var depositAddresses = lodash.flatMap(depositWallets, (it) => { return it.address })
    var options = 'https://blockexplorer.com/api/addrs/' + depositAddresses.join(',') + '/txs'

    Unirest.get(options)
    .send()
    .timeout(10000)
    .end(function (response) {
      if (!response.body || !response.body.items) {
        return callback('nothing in response')
      }
      let waiting = 0
      const checkWaiting = function () {
        waiting--
        if (waiting === 0) {
          return callback(null, 'done')
        }
      }
      // console.log(response.body.items.length)
      Async.everySeries(response.body.items, function(txresponse, everyDone) {

        waiting++
        Async.auto({
          checkTx: function (done) {
            var thisDepositAddress

            if (txresponse.isCoinBase && txresponse.confirmations < 100) {
              // do nothing
              console.log('WARNING, coinbase tx')
              return done('coinbase tx')
            } else if (txresponse.confirmations > 0) {
              // console.log('examining new deposit')
              var totalValue = 0

              var thisDeposit
              for (var x = 0; x < txresponse.vout.length; x++) {
                if (txresponse.vout[x].scriptPubKey.addresses.length > 1) {
                  throw ' well this should never happen '
                  return done('no vouts')
                }
                var thisAddr = txresponse.vout[x].scriptPubKey.addresses[0]
                // console.log(txresponse.vout[x])
                thisDepositAddress = lodash.find(depositWallets, {address: thisAddr})
                // this is just double checking to make sure this is actually the right address and wallet
                // commonly it's checking outgoing TXs and that's why it will skip
                if (thisDepositAddress && thisAddr === thisDepositAddress.address) {
                  const btcAmount = Bitcore.Unit.fromBTC(txresponse.vout[x].value)
                  const usdValue = server.settings.app.exchangeRates["USDT_BTC"].highestBid*btcAmount.toBTC()
                  thisDeposit = {
                    currency: 'bitcoin',
                    txid: txresponse.txid,
                    createdAt: new Date(),
                    type: 'deposit',
                    address: thisAddr,
                    userId: thisDepositAddress.userId,
                    amount: btcAmount.toSatoshis(),
                    usdValue: usdValue
                  }
                  break
                }
              }
              if (!thisDeposit) {
                return done('!thisDeposit')
              }
              Transaction.count({type: 'deposit', txid: thisDeposit.txid}, function (err, result) {
                if (result) {
                  // already counted, do nothing
                  return done('alreadyCounted')
                }
                return done(null, {thisDepositAddress: thisDepositAddress, thisDeposit: thisDeposit})
              })
            } else {
              // unconfirmed transactions. do nothing
              return done('unconfirmed')
            }
          },
          processDeposit: ['checkTx', function (results, done) {
            if (!results.checkTx) {
              return done('no txs found')
            }
            var thisDeposit = results.checkTx.thisDeposit
            var thisDepositAddress = results.checkTx.thisDepositAddress

            sendBitcoinTx(thisDepositAddress, thisDeposit.address, thisDeposit.amount, process.env.BTC_HOT_WALLET, process.env.BTC_COLD_WALLET, function (transaction) {
              if (transaction.err) {
                console.error("Error sending to cold wallet: " + transaction.err);
              } else {
                console.log('sent bitcoin to cold wallet', transaction.hash)
                thisDeposit.houseTx = transaction.hash
              }

              thisDeposit.amount = thisDeposit.amount
              return processDeposit(thisDeposit, done)
            })
          }]
        }, (err, results) => {
          // if (err) {
          //   console.log(err)
          // }
          everyDone(null, results)
        })
      }, (err, results) => {
        return callback(null, results)
      });
    }, function (error) {
      return callback('BLOCKCHAIN FETCH ERROR')
    })
  }

  const processDeposit = (thisDeposit, done) => {
    Transaction.create(thisDeposit, (err, deposit) => {
      if (err) {
        console.error('error creating deposit in DB.', err, thisDeposit)
        return done(err)
      }
      Wallet.findOne({userId: thisDeposit.userId, currency: thisDeposit.currency}, function (err, wallet) {
        if (err || !wallet) {
          console.error('error updating balance after deposit', err, thisDeposit)
          return done(err)
        }
        DepositAddress.findOneAndUpdate({address: thisDeposit.address}, {$inc:{totalReceived: thisDeposit.amount}}, function(err, result) {
          if (err || !result) {
            return done('FAILED AT DEPOSIT WALLET UPDATE', err)
          }
          Wallet.balance(wallet.userId, thisDeposit.currency, thisDeposit.amount, function (err, newWallet) {
            if (err) {
              console.log(err)
              return done('FAILED AT WITHDRAW WALLET UPDATE')
            }
            for (var ps in server.settings.app.users) {
              var psock = server.settings.app.users[ps]
              if (psock.userId === thisDeposit.userId) {
                let newWalletObj = {}
                newWalletObj[newWallet.currency] = newWallet
                io.sockets.connected[ps] && io.sockets.connected[ps].emit('wallet-update', newWalletObj)
                io.sockets.connected[ps] && io.sockets.connected[ps].emit('deposit', thisDeposit)
                break
              }
            }
            console.log('DEPOSIT SUCCESSFUL', thisDeposit)
            return done()
          })

        })

      })
    })
  }

  server.method('checkDepositsBitcoin', function (request, reply) {
    // console.log("CHECKING TXs");
    Async.auto({
      exchangeRate: function (callback) {
        polo.getPair(null, null, function (err, rates) {
          if (err || !rates) {
            console.error(err)
            return callback('error getting exchange rates')
          }
          if(!rates || !rates["USDT_BTC"]) {
            return callback('exchange rates are blank')
          }
          server.settings.app.exchangeRates = rates
          Socket.serverSocketBroadcast('exchange-rates', rates)

          // console.log("BTC/USD Exchange Rate: $"+ rates["USDT_BTC"].highestBid);
          // console.log("BTC/XXX Exchange Rate: BTC "+ rates["BTC_XXX"].highestBid);
          return callback(null, rates)
        })
      },
      depositWallets: ['exchangeRate', (results, callback) => {
        var now = new Date().getTime()
        var fiveDaysOld = new Date(now - 86400000 * 5)
        const findWhich = {createdAt: {$gt: fiveDaysOld}, currency: 'bitcoin'}
        let depositWallets = []

        DepositAddress.find(findWhich, function (err, depositWallets) {
          if (!depositWallets || err) {
            return callback('nothing to do', err)
          }
          return callback(null, depositWallets)
        })
      }],
      btcdeposits: ['depositWallets', (results, callback) => {
        if (!results.depositWallets.length) {
          return callback('no deposit wallets')
        }

        var options = 'https://blockexplorer.com/api/status?q=getLastBlockHash'
        Unirest.get(options)
        .send()
        .timeout(10000)
        .end(function (response) {
          if (!response.body || !response.body.lastblockhash) {
            return callback()//'nothing in JSON response for block explorer')
          }
          if (server.settings.app.lastBlockHash !== response.body.lastblockhash) {
            server.settings.app.lastBlockHash = response.body.lastblockhash
            // console.log('NEW BLOCK ', server.settings.app.lastBlockHash)
            const theseWallets = lodash.filter(results.depositWallets, {currency: 'bitcoin'})
            if (!theseWallets.length) {
              return callback('no bitcoin addresses to check')
            }
            return checkBitcoinTxs(theseWallets, callback)
          } else {
            return callback()//'waiting until next block to check transactions')
          }
        })
      }]
    }, (err, results) => {
      if (err) {
        // console.log(err)
      }
      return reply()
    })
  })

  next()
}

exports.register.attributes = {
  name: 'task-deposits-bitcoin'
}
