'use strict'
const Joi = require('joi')
const MongoModels = require('mongo-models')

class MongoModelsX extends MongoModels {
  static create (record,reply) {
    var self = this;
    record.createdAt = new Date();
    Joi.validate(record, this.schema, function (errValidate, validatedRecord) { 
      if (errValidate) {
        return reply(errValidate);
      }

      self.insertOne(validatedRecord, (err, docs) => {
        if (err) {
            console.log(err);
            return reply(err);
        }
        return reply(null, docs[0]);                   
      });
    });
  }
  static findByIdValidateAndUpdate (id, update, reply) {
    var self = this
    Joi.validate(update, this.schema, function (errValidate, validatedRecord) {
      if (errValidate) {
          return reply(errValidate);
      }

      self.findByIdAndUpdate(id, validatedRecord, (err, result) => {
        if (err) {
            return reply(err);
        }
        reply(null,result);
      });
    });
  }
}

module.exports = MongoModelsX