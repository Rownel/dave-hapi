'use strict'
const Async = require('async')
const AuthPlugin = require('../auth')
const Boom = require('boom')
const Joi = require('joi')

const internals = {}

internals.applyRoutes = function (server, next) {
  const Transaction = server.plugins['hapi-mongo-models'].Transaction
  const User = server.plugins['hapi-mongo-models'].User

  server.route({
    method: 'GET',
    path: '/transactions',
    config: {
      auth: {
        strategy: 'simple',
        scope: 'admin'
      },
      validate: {
        query: {
          fields: Joi.string(),
          sort: Joi.string().default('-createdAt'),
          limit: Joi.number().default(100),
          page: Joi.number().default(1)
        }
      },
      pre: [
        AuthPlugin.preware.ensureAdminGroup('root')
      ]
    },
    handler: function (request, reply) {
      const query = {}
      const fields = request.query.fields
      const sort = request.query.sort
      const limit = request.query.limit
      const page = request.query.page

      Transaction.pagedFind(query, fields, sort, limit, page, (err, results) => {
        if (err) {
          return reply(err)
        }

        reply(results)
      })
    }
  })

  server.route({
    method: 'GET',
    path: '/transactions/{id}',
    config: {
      auth: {
        strategy: 'simple',
        scope: 'admin'
      },
      pre: [
        AuthPlugin.preware.ensureAdminGroup('root')
      ]
    },
    handler: function (request, reply) {
      Transaction.findById(request.params.id, (err, transaction) => {
        if (err) {
          return reply(err)
        }

        if (!transaction) {
          return reply(Boom.notFound('Document not found.'))
        }

        reply(transaction)
      })
    }
  })


  next()
}

exports.register = function (server, options, next) {
  server.dependency(['auth', 'hapi-mongo-models'], internals.applyRoutes)

  next()
}

exports.register.attributes = {
  name: 'transactions'
}
