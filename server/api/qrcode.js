'use strict'
const Boom = require('boom')
const yaqrcode = require('yaqrcode')
const internals = {}

internals.applyRoutes = function (server, next) {
  server.route({
    method: 'GET',
    path: '/qrcode/{data}',
    config: {
      plugins: {
        'hapi-attempts-limiter': {
            genericRateLimiter: true,
            limit: 200,
            duration: 60
        }
      }      
    },
      handler: function(request,reply) {
        const data = yaqrcode(request.params.data,{size:250});

        function decodeBase64Image(dataString) {
          var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
            response = {};

          if (matches.length !== 3) {
            return reply(Boom.badRequest('Invalid input string'));
          }

          response.type = matches[1];
          response.data = new Buffer(matches[2], 'base64');

          return response;
        }

        var imageBuffer = decodeBase64Image(data);
        return reply(imageBuffer.data).type('image/gif');
      }
  });

  next()
}

exports.register = function (server, options, next) {
  server.dependency([], internals.applyRoutes)

  next()
}

exports.register.attributes = {
  name: 'qrcode'
}
