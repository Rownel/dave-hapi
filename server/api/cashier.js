'use strict'
const Boom = require('boom')
const Socket = require('./socket')
const Bitcore = require('bitcore-lib')
const Async = require('async')
const polo = require('../lib/polo.js')
const internals = {}

internals.applyRoutes = function (server, next) {

  const DepositAddress = server.plugins['hapi-mongo-models'].DepositAddress;
  const Wallet = server.plugins['hapi-mongo-models'].Wallet;
  const Transaction = server.plugins['hapi-mongo-models'].Transaction;
  const Withdraw = server.plugins['hapi-mongo-models'].Withdraw;
  const io = server.plugins['hapi-io'].io;
  server.route({
    method: 'GET',
    path: '/exchange-rates',
    config: {
      plugins: { 
        'hapi-io': {
          event: 'exchange-rates',
          post: function(ctx,next) {
            ctx.socket.emit('exchange-rates', ctx.res.result)
            next();
          }
        },
        'hapi-attempts-limiter': {
            genericRateLimiter: true,
            limit: 300,
            duration: 600
        }
      }     
    },
    handler: function(request,reply) {
      polo.getPair(null, null, function (err, rates) {
        if (err || !rates) {
          console.error(err)
          return reply(Boom.badRequest('error getting exchange rates'))
        }
        if(!rates || !rates["USDT_BTC"]) {
          return reply(Boom.badRequest('exchange rates are blank'))
        }
        server.settings.app.exchangeRates = rates

        // console.log("BTC/USD Exchange Rate: $"+ rates["USDT_BTC"].highestBid);
        // console.log("BTC/XXX Exchange Rate: BTC "+ rates["BTC_XXX"].highestBid);
        return reply(rates)
      })      
    }
  })
  server.route({
    method: 'GET',
    path: '/deposit/{currency}',
    config: {
      auth: {
        strategy: 'simple',
        scope: 'account'
      },
      plugins: {
        'hapi-attempts-limiter': {
            genericRateLimiter: true,
            limit: 300,
            duration: 600
        }
      }
    },
    handler: function(request,reply) {
      DepositAddress.create(request.auth.credentials.user,request.params.currency, (err,address) => {
        if(err || !address) {
          console.error(err);
          return reply(Boom.badRequest('Unable to get address'));
        }
        reply({address:address});
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/withdraws/my',
    config: {
      auth: {
        strategy: 'simple',
        scope: 'account'
      },
      plugins: {
        'hapi-attempts-limiter': {
            genericRateLimiter: true,
            limit: 300,
            duration: 600
        }     
      }
    },
    handler: function (request, reply) {
      Withdraw.find({userId: request.auth.credentials.user._id.toString(), approvedAt: {$exists:true}}, function(err,records) {
        if(err || !records) {
          return reply(Boom.badRequest("No withdrawals found"))
        }
        return reply (records)
      });
    }
  });
  server.route({
    method: 'GET',
    path: '/deposits/my',
    config: {
      auth: {
        strategy: 'simple',
        scope: 'account'
      },
      plugins : {
        'hapi-attempts-limiter': {
            genericRateLimiter: true,
            limit: 300,
            duration: 600
        }          
      }
    },
    handler: function (request, reply) {
      Transaction.find({type: 'deposit', userId: request.auth.credentials.user._id}, function(err,records) {
        if(err || !records) {
          return reply(Boom.badRequest("No deposits found"))
        }
        return reply (records)
      });
    }
  });
  server.route({
    method: 'PUT',
    path: '/wallet/my',
    config: {
      auth: {
        strategy: 'simple',
        scope: 'account'
      },
      plugins: {
        'hapi-attempts-limiter': {
            genericRateLimiter: true,
            limit: 300,
            duration: 600
        }                               
      }
    },
    handler: function (request, reply) {
      const id = request.auth.credentials.user._id.toString();

      if(request.payload.currency === 'bitcoin') {
        if(!Bitcore.Address.isValid(request.payload.address, Bitcore.Networks.livenet)) {
          return reply(Boom.badRequest("Invalid wallet address"));
        }
      } else {
        return reply(Boom.badRequest("Withdrawals for this currency are not supported"));
      }
      const update = {$set: {address: request.payload.address}}
      Wallet.update({userId: id, currency: request.payload.currency}, update, (err, wallet) => {
        if (err) {
          console.error(err)
          return reply(Boom.badRequest("Unable to update wallet address"));
        }

        for(var ps in server.settings.app.users) {
          var psock = server.settings.app.users[ps];
          if(psock.userId === id) {
            var walletObj = {}
            walletObj[wallet.currency] = wallet
            io.sockets.connected[ps] && io.sockets.connected[ps].emit('wallet-update',walletObj)
            break;
          }
        }     
        reply(wallet);
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/withdraw',
    config: {
      plugins: { 
        'hapi-attempts-limiter': {
            genericRateLimiter: true,
            limit: 100,
            duration: 600
        },          
        'hapi-io': {
        event: 'withdraw',
        post: function(ctx,next) {
          Socket.checkSocket(ctx, function(err) {
            if(err) {
              return false;
            }
            const creds = ctx.res.result.credentials
            const user = creds.user;
            const id = user._id.toString()

            Async.auto({
                checks: function(callback) {
                  if(!ctx.data) {
                    ctx.data = {error:"Invalid parameters"}
                    return callback(ctx.data)              
                  }
                  if(server.settings.app.withdrawsActive[id]) {
                    ctx.data = {error:"Account Locked"}
                    return callback(ctx.data)
                  }
                  if(!Bitcore.Address.isValid(ctx.data.address, Bitcore.Networks.livenet)) {
                    ctx.data = {error:"Invalid Wallet Address"}
                    return callback(ctx.data);
                  }                  
                  // transform/validate amount
                  ctx.data.amount = Bitcore.Unit.fromBTC(ctx.data.amount);
                  if(isNaN(ctx.data.amount.toSatoshis())) {
                    ctx.data = {error:"Invalid amount "+ctx.data.amount}
                    return callback(ctx.data)         
                  }            
                  ctx.data.amount = ctx.data.amount.toSatoshis()
                  if(ctx.data.amount < 100000) {
                    ctx.data = {error: "Minimum withdrawal amount 0.001"}
                    return callback(ctx.data)
                  }
                  server.settings.app.withdrawsActive[id] = true
                  callback()
                },
                wallet: ['checks', function(result, callback) {
                  Wallet.findOne({userId: user._id.toString(), currency: ctx.data.currency}, function(err, wallet) {
                    if(err || !wallet) {
                      ctx.data = {error: "Cannot retrieve wallet"}
                      return callback(ctx.data)
                    }
                    // if(!wallet.address) {
                    //   ctx.data = {error: "Please set a withdrawal address first"}
                    //   return callback(ctx.data)
                    // }
                    if(wallet.balance < ctx.data.amount) {
                      ctx.data = {error: "Insufficient Funds"}
                      return callback(ctx.data)
                    }                              

                    return callback(null, wallet)
                  })
                }],
                withdraw: ['wallet', function(results, callback) {
                  let withdrawObj = {
                    userId: user._id.toString(),
                    amount: ctx.data.amount,
                    currency: ctx.data.currency,
                    createdAt: new Date()
                  }

                  Wallet.balance(user._id.toString(), ctx.data.currency, -(ctx.data.amount), function(err,newWallet) {
                    ctx.socket.emit('wallet-update', newWallet)
                    server.settings.app.withdrawsActive[id] = false
                    if(err) {
                      ctx.data = {error:"Unable to update balance. Please contact support immediately."}
                      return ctx.socket.emit('withdraw-error', ctx.data)
                    }
                    if(!newWallet) {
                      ctx.data = {error:"Unable to retrieve wallet. Please contact support immediately."}
                      return ctx.socket.emit('withdraw-error', ctx.data)
                    }
                    Withdraw.create(withdrawObj, (err,withdraw) => {
                      if(err || !withdraw) {
                        console.error("database error creating withdrawal", err);
                        ctx.data = {error:"database error creating withdrawal"}
                        return ctx.socket.emit('withdraw-error', ctx.data)
                      } else {
                        return ctx.socket.emit('withdraw-success', {withdraw:withdraw})
                      }
                    });
                  });                  
                }]
            }, function(err, results) {
              server.settings.app.withdrawsActive[id] = false
              if(err) {
                console.log(err)
                return ctx.socket.emit('withdraw-error', err)
              }
            })

          });
          next();
        }
      }}
    },
    handler: function(request,reply) {
      Socket.loginSocket(request, function(creds) {
        reply(creds)
      })
    }
  });
  next()
}

exports.register = function (server, options, next) {
  server.dependency(['hapi-mongo-models'], internals.applyRoutes)

  next()
}

exports.register.attributes = {
  name: 'cashier'
}
