'use strict'
const Async = require('async')
const Boom = require('boom')
const Config = require('../../config')
const lodash = require('lodash')
const internals = {}

internals.applyRoutes = function (server, next) {

  const Session = server.plugins['hapi-mongo-models'].Session;
  const User = server.plugins['hapi-mongo-models'].User;
  const IpLog = server.plugins['hapi-mongo-models'].IpLog;
  const Wallet = server.plugins['hapi-mongo-models'].Wallet;
  const io = server.plugins['hapi-io'].io;

  exports.deletePlayerSocket = function(socket) {
    console.log('deleted socket',socket.id)
    delete server.settings.app.users[socket.id]
  }
  io.sockets.on('connection', function(socket) {
    socket.on('disconnect', function() {
      socket.removeAllListeners()
      if(!server.settings.app.users[socket.id]) {
        return;
      } 
      return exports.deletePlayerSocket(socket)
    });
  });

// unfortunately all this code is copied and duplicated from hapi-basic-auth/index and validateFunc
  // I'm sure there's a way to do it better, but I don't have time for refactor now
  exports.authSocket = function(request, reply) {
    const authorization = request.query.authHeader
    const parts = authorization.split(/\s+/);

    if (parts[0].toLowerCase() !== 'basic') {
      return reply("no session security data");
    }
    if (parts.length !== 2) {
      return reply(Boom.badRequest('Bad HTTP authentication header format', 'Basic'));
    }
    const credentialsPart = new Buffer(parts[1], 'base64').toString();
    const sep = credentialsPart.indexOf(':');
    if (sep === -1) {
      return reply(Boom.badRequest('Bad header internal syntax', 'Basic'));
    }
    const username = credentialsPart.slice(0, sep);
    const password = credentialsPart.slice(sep + 1);

    Async.auto({
      session: function (done) {
        Session.findByCredentials(username, password, done);
      },
      user: ['session', function (results, done) {
        if (!results.session) {
          return done();
        }
        User.findById(results.session.userId, done);
      }],
      roles: ['user', function (results, done) {
        if (!results.user) {
          return done();
        }
        results.user.hydrateRoles(done);
      }],
      scope: ['user', function (results, done) {
        const ip = request.headers['x-forwarded-for'] || request.info.remoteAddress;
        if (!results.user || !results.user.roles) {
          // console.log("no user found");
          return done();
        }
        // don't do checks for IPs for admins
        if(results.user.roles.admin) {
          return done(null, Object.keys(results.user.roles));
        }
        IpLog.find({ip:ip,userId:{$ne:results.user._id}},
          function(error,usersByIp) {
          var allIps = [];
          var allUsers = [];
          for(var i=0;i<usersByIp.length;i++) {
            allIps.push(usersByIp[i].ip);
            allUsers.push(usersByIp[i].userId);
          }
          if(error) {
            console.log("ip lookup error", error);
            return done("ip lookup error");
          }
          if(usersByIp.length > Config.get('/maxUsersByIp')) {
            console.log("max users by IP limit met: socket", usersByIp);
            return User.updateMany({ip:{$in:allIps}},
              {$set:{isActive:false}},
              function(err,updateResults) {
                return done("too many logins from this IP");
            });
          }
          IpLog.create({userId:results.user._id.toString(),ip:ip}, function(err,result) {
            if(err) {
              console.log("iplog create error",err);
              return done("iplog create error");
            } else {
              return done(null, Object.keys(results.user.roles));
            }
          });
        });
      }]
    }, (err, results) => {
      let credentials = results || null
      let isValid = Boolean(results.user)
      if (err) {
        return reply(err);
      }
      if (!results.session) {
        return reply(null, false);
      }
      if (!isValid) {
        return reply('Bad session security data');
      }
      if (!credentials ||
        typeof credentials !== 'object') {
        return reply('Bad credentials object received for Basic auth validation');
      }
      // Authenticated
      return reply(null, { credentials: credentials });
    });
  }
  exports.loginSocket = function(request, cb) {
   // the authHeader is passed up in the query on socket requests only.
   // this whole request is only used for the socket 'game' event,
   // so if somebody tries to hit it with an XHR call, just quit
   if(!request.query.authHeader) {
    console.error('no authheader')
    return cb("no auth header")
   }
   exports.authSocket(request, function(err, credentials) {
    if(err || !credentials) {
      console.error('login socket error', err)
      return cb(err)
    }
    console.log('login socket success', credentials.credentials.user._id)
    cb(null, credentials)
   })
  }
  exports.checkSocket = function(ctx, cb) {
    if(!ctx.res.result || !ctx.res.result.credentials) {
     // console.error('socket check failed')
     return cb("socket check failed");
    }
    var creds = ctx.res.result.credentials;
    if(!server.settings.app.users[ctx.socket.id]) {
      const exists = lodash.find(server.settings.app.users, function(o, i) { 
        return i !== ctx.socket.id && o.userId === creds.user._id.toString()
      });
      if(exists) {
        console.log("???", ctx.socket.id, exists)
        ctx.socket.emit('multiple-sockets')
        return cb("multiple sockets")
      }
      server.settings.app.users[ctx.socket.id] = {userId: creds.user._id.toString(), name: creds.user.username}
      User.findByIdAndUpdate(creds.user._id.toString(),
        {$set:{seenAt: new Date()}}, function(err, user) {
          Wallet.find({userId: creds.user._id.toString()}, function(err,wallets) {
            let walletsObj = {}
            lodash.each(wallets, function(wallet) {
              walletsObj[wallet.currency] = wallet
            })            
            ctx.socket.emit('wallet-update', walletsObj)
            ctx.socket.emit('socket-initialized')
            return cb()
          });
        });
    } else {
      Wallet.find({userId: creds.user._id.toString()}, function(err,wallets) {
        let walletsObj = {}
        lodash.each(wallets, function(wallet) {
          walletsObj[wallet.currency] = wallet
        })
        ctx.socket.emit('wallet-update', walletsObj)
        ctx.socket.emit('socket-initialized')
        return cb()
      });      
    }
    ctx.socket.emit('user-update', creds.user)
  }
  exports.serverSocketBroadcast = function(eventName,eventData) {
    if(io) {
      io.emit(eventName,eventData);
    }
  };
  server.route({
    method: 'GET',
    path: '/start',
    config: {
      plugins: {
        'hapi-attempts-limiter': {
            genericRateLimiter: true,
            limit: 300,
            duration: 600
        },
        'hapi-io': {
          event: 'start',
          post: function(ctx,next) {
            exports.checkSocket(ctx, function(err) {
              ctx.socket.emit('socket-initialized')
              if(err) {
                console.log("socket login error", err)
              } else {
                const creds = ctx.res.result.credentials
                ctx.socket.emit('user-update',creds.user)                
              }
              next();
            });
          }
        }
      }
    },
    handler: function (request, reply) {
      exports.loginSocket(request, function(err, creds) {
        if(err) {
          return reply(Boom.badRequest(err))
        }
        reply(creds)
      })
    }
  });  
  next()
}

exports.register = function (server, options, next) {
  server.dependency([], internals.applyRoutes)
  next()
}

exports.register.attributes = {
  name: 'socket'
}
