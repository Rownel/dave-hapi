'use strict';
const MongoModelsX = require('../lib/mongo-models-x')
const Config = require('../../config')
const Joi = require('joi');
const ObjectAssign = require('object-assign');

class Wallet extends MongoModelsX {
  static balance (userId, currency, amount, callback) {
    if(!amount || isNaN(amount)) {
      console.error('amount NaN or not found')
      return callback('amount NaN or not found')
    }
    var self = this
    this.findOne({userId: userId, currency: currency}, function(err, wallet) {
      if(err) {
        return callback(err)
      }
      if(!wallet) {
        return callback(new Error("no wallets found for this user"))
      }
      if(wallet.locked) {
        return callback(new Error("wallet is locked"))
      }
      if(!wallet.balance) {
        wallet.balance = 0;
      }
      // THIS IS WHERE THE USER'S ACCOUNT BALANCE ACTUALLY GETS UPDATED
      if(wallet.balance + amount < 0) {
        return callback(new Error("insufficient funds"))
      }      
      wallet.locked = true
      self.findByIdValidateAndUpdate(wallet._id, wallet, function(err,result) {
        if(err || !result) {
          return callback(err)
        }
        wallet.balance += amount
        self.findByIdValidateAndUpdate(wallet._id, wallet, function(err,result) {
          if(err || !result) {
            return callback(err)
          }
          wallet.locked = false
          self.findByIdValidateAndUpdate(wallet._id, wallet, function(err,result) {
            if(err || !result) {
              return callback(err)
            }
            callback(null, result)
          })
        })
      })
    })
  };    
}

Wallet.collection = 'wallets';

Wallet.schema = Joi.object().keys({
  _id: Joi.object(),
  userId: Joi.string().required(),
  currency: Joi.string().required(),
  createdAt: Joi.date(),
  balance: Joi.number(),
  lastBalanceCheck: Joi.date(),
  locked: Joi.boolean()
});

Wallet.indexes = [
  { key: { 'currency': 1 } },
  { key: { 'lastBalanceCheck': -1 } },
  { key: { 'balance': 1 } },
  { key: { 'userId': 1, currency: 1 }, unique: true }
];

module.exports = Wallet;