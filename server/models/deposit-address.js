'use strict';
const MongoModelsX = require('../lib/mongo-models-x')
const Config = require('../../config')
const Joi = require('joi');
const ObjectAssign = require('object-assign');
const Bitcore = require('bitcore-lib');
const lodash = require('lodash')

class DepositAddress extends MongoModelsX {
  static getAddress(user, currency, i, callback) {
    if(currency === 'bitcoin') {
      var HDPrivateKey = Bitcore.HDPrivateKey;
      var HDPublicKey = Bitcore.HDPublicKey;
      var Address = Bitcore.Address;
      var Networks = Bitcore.Networks;
      var hdPrivateKey = new HDPrivateKey.fromSeed(process.env.SEEDHEX);
      var hard = hdPrivateKey.derive("m/0'/0");
      var derivedAddress = new Address(hard.derive("m/"+i).publicKey, Networks.livenet);
      var newAddress = derivedAddress.toString()
      // console.log("child address of m/0'/0 with index "+i,derivedAddress.toString());
      return callback(newAddress)
    } else {
       return callback()
    }
  }
  static create (user,currency,reply) {
    var self = this
    this.findOne({currency:currency}, {sort:{height: -1},limit:1}, (err,results) => {
      // console.log(results)
      var i = 1;

      if(results) {
        i = results.height+1
      }

      self.getAddress(user, currency,i, (newAddress) => {
        if(!newAddress) {
          return reply({msg:"Error getting address or currency not supported!"});
        }

        var document = { 
          createdAt: new Date(),
          currency: currency,
          height: i,
          address: newAddress,
          userId: user._id.toString()
        }
        self.insertOne(document, (err,docs) => {
          if(err || !docs) {
            console.error(err);
            return reply({msg:"Error adding address"});
          }
          return reply(null,docs[0].address);
        });
      })
    });
  };   
}

DepositAddress.collection = 'depositwallets';

DepositAddress.schema = Joi.object().keys({
  _id: Joi.object(),
  currency: Joi.string().required(),
  createdAt: Joi.date(),
  userId: Joi.string(),
  address: Joi.string().required().allow(''),
  height: Joi.number().integer().required(),
  totalReceived: Joi.number()
});

DepositAddress.indexes = [
  { key: { 'currency': 1 } },
  { key: { 'height': 1 } },
  { key: { 'address': 1 } },
  { key: { 'userId': -1 } },
  { key: { 'createdAt': -1 } }
];

module.exports = DepositAddress;