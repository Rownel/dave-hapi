'use strict'
const Async = require('async')
const Joi = require('joi')
const MongoModelsX = require('../lib/mongo-models-x')
class Withdraw extends MongoModelsX {
}

Withdraw.collection = 'withdraw';

Withdraw.schema = Joi.object().keys({
  _id: Joi.object(),
  userId: Joi.string().required(),
  amount: Joi.number().integer().required(),
  currency: Joi.string().required(),
  createdAt: Joi.date().required(),
  approvedAt: Joi.date(),
  rejectedAt: Joi.date()
});


Withdraw.indexes = [
  { key: { 'userId': -1 } },
  { key: { 'createdAt': -1 } },
  { key: { 'approvedAt': -1 } },
  { key: { 'rejectedAt': -1 } },
  { key: { 'amount': -1 } }
];

module.exports = Withdraw