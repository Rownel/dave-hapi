'use strict'
const Async = require('async')
const Joi = require('joi')
const MongoModelsX = require('../lib/mongo-models-x')
class Transaction extends MongoModelsX {
}

Transaction.collection = 'transactions';

Transaction.schema = Joi.object().keys({
  _id: Joi.object(),
  currency: Joi.string().required(),
  type: Joi.string(),
  createdAt: Joi.date(),
  userId: Joi.string(),
  address: Joi.string().required(),
  txid: Joi.string().required(),
  houseTx: Joi.string(),
  amount: Joi.number(),
  usdValue: Joi.number()
});


Transaction.indexes = [
  { key: { 'currency': 1 } },
  { key: { 'type': 1 } },
  { key: { 'usdValue': 1 } },
  { key: { 'createdAt': -1 } },
  { key: { 'userId': -1 } },
  { key: { 'address': 1 } },
  { key: { 'txid': -1 }, unique: true},
  { key: { 'amount': 1 } }
];
module.exports = Transaction
