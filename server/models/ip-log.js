'use strict'
const Async = require('async')
const Joi = require('joi')
const MongoModelsX = require('../lib/mongo-models-x')
class IpLog extends MongoModelsX {
	static create(event, reply) {
	    var self = this;
	    Joi.validate(event, IpLog.schema, function (errValidate, value) { 
	        if (errValidate) {
	            return reply(errValidate);
	        }

	        Async.auto({
	            clean: function (done, results) {
	                self.deleteOne(event, done);
	            }, newLog: function (done, results) {

	                self.insertOne(event, done);
	            }
	        }, (err, results) => {

	            if (err) {
	                return callback(err);
	            }

	            reply(null, event);
	        });
	    });		
	}
}

IpLog.collection = 'iplog';

IpLog.schema = Joi.object().keys({
    ip: Joi.string().required(),
    userId: Joi.string().required(),
    createdAt: Joi.date()
});

IpLog.indexes = [
    { key: { 'userId': -1 } },
    { key: { 'ip': -1 } }
];
module.exports = IpLog
