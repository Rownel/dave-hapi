const auth = {
	token: process.env.LOGGLY_TOKEN,
	subdomain: process.env.LOGGLY_SUBDOMAIN,
	tags: [process.env.APP_NAME],
	json:true
};

module.exports = {auth};
