'use strict'
const Composer = require('./index')
const testPromises = require('./server/tasks/test-promises')
const corsHeaders = require('hapi-cors-headers')
Composer((err, server) => {
  if (err) {
    throw err
  }
  server.ext('onPreResponse', corsHeaders)

  server.method('testPromises', testPromises)

  server.start(() => {
    console.log('Started the plot device on port ' + server.info.port)
  })
})
