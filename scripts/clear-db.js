'use strict'
const Async = require('async')
const Mongodb = require('mongodb')
const MongoModels = require('mongo-models')
const Account = require('../server/models/account')
const AdminGroup = require('../server/models/admin-group')
const Admin = require('../server/models/admin')
const AuthAttempt = require('../server/models/auth-attempt')
const Session = require('../server/models/session')
const Status = require('../server/models/status')
const User = require('../server/models/user')  
const IpLog = require('../server/models/ip-log')
const Wallet = require('../server/models/wallet')
const DepositAddress = require('../server/models/deposit-address')
const Transaction = require('../server/models/transaction')
const Withdraw = require('../server/models/withdraw') 
// do NOT try this
// const NoteEntry = require('../server/models/note-entry') 
// const StatusEntry = require('../server/models/status-entry') */
console.log(process.env.MONGODB_URI)
MongoModels.connect(process.env.MONGODB_URI, {}, function(err, res) {
  Async.parallel([
    Account.deleteMany.bind(Account, {}),
    AdminGroup.deleteMany.bind(AdminGroup, {}),
    Admin.deleteMany.bind(Admin, {}),
    AuthAttempt.deleteMany.bind(AuthAttempt, {}),
    Session.deleteMany.bind(Session, {}),
    Status.deleteMany.bind(Status, {}),
    User.deleteMany.bind(User, {}),
    IpLog.deleteMany.bind(IpLog, {}),
    Wallet.deleteMany.bind(Wallet, {}),
    DepositAddress.deleteMany.bind(DepositAddress, {}),
    Transaction.deleteMany.bind(Transaction, {}),
    Withdraw.deleteMany.bind(Withdraw, {}),  
    // do NOT try this
    // NoteEntry.deleteMany.bind(NoteEntry, {}),
    // StatusEntry.deleteMany.bind(StatusEntry, {})    
  ], function(err, result) {
    console.log(err, result)
    process.exit(0)
  })
});