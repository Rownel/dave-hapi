FROM node:7.10.0

ARG API_PORT
ARG APP_NAME
ENV APP_USER "app"
ENV HOME /home/$APP_USER
ENV APP_DIR $HOME/$APP_NAME
RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "vim"]
RUN useradd --user-group --create-home --shell /bin/false $APP_USER

RUN mkdir $APP_DIR
WORKDIR $APP_DIR

RUN apt-get update
RUN npm install -g gulp

COPY . $APP_DIR

RUN chown -R $APP_USER:$APP_USER $HOME/*
RUN npm cache clean && npm install
USER $APP_USER
WORKDIR $APP_DIR

EXPOSE $API_PORT

CMD ["make", "live"]
