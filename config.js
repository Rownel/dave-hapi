'use strict'
const Confidence = require('confidence')
const Dotenv = require('dotenv')

Dotenv.config({ silent: true })

const criteria = {
  env: process.env.NODE_ENV
}

const config = {
  $meta: 'This file configures the plot device.',
  projectName: 'Frame',
  port: {
    web: {
      $filter: 'env',
      test: 9090,
      $default: process.env.API_PORT
    }
  },
  maxUsersByIp: 99999,
  authAttempts: {
    forIp: 50,
    forIpAndUser: 7
  },
  cookieSecret: {
    $filter: 'env',
    production: process.env.COOKIE_SECRET,
    $default: '!lkjsadfp98732q4iuawe~oiuwerlkhusdf~lkhdsfsdfsdf20232!'
  },
  hapiMongoModels: {
    mongodb: {
      uri: {
        $filter: 'env',
        production: process.env.MONGODB_URI,
        test: 'mongodb://localhost:27017/frame-test',
        $default: process.env.MONGODB_URI
      }
    },
    autoIndex: true
  },
  nodemailer: {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: 'webmaster@gmail.com',
      pass: process.env.SMTP_PASSWORD
    }
  },
  system: {
    fromAddress: {
      name: 'App',
      address: 'webmaster@gmail.com'
    },
    toAddress: {
      name: 'App',
      address: 'webmaster@gmail.com'
    }
  }
}

const store = new Confidence.Store(config)

exports.get = function (key) {
  return store.get(key, criteria)
}

exports.meta = function (key) {
  return store.meta(key, criteria)
}
