## Development Help

Application can run without Docker. There may some issues with redis server.

### Before run 

- Please install latest NodeJS. If you are using `nvm` please 
make sure that, you have installed latest version and set latest as default 

```
#shell 
nvm install 8.4.0 // replace 8.4.0 with current latest 
nvm use 8.4.0
nvm alias default 8.4.0 // current latest 
```

- Create `redis.config` file and bind ip to `0.0.0.0`, This not a 
good option, but if you facing any connect issues try this. check
`.env` file for **REDIS_HOST** variable and use same port in `redis.config`

- If you face any mongo db connection issues, first check `.env` file
 **MONGODB_URI**, if you have any issues with current settings, change to `mongodb://0.0.0.0:27017` and edit mongodb.config file 
 `/etc/mongodb.conf` and change 

```
bind_ip = 0.0.0.0
#port = 27017
```
restart server 

### Normal run 

- run `make dev` 

