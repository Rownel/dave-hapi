include .env
export $(shell sed 's/=.*//' .env)
live:
	export NODE_ENV=production
	export NPM_CONFIG_PRODUCTION=false
	node server.js
dev:
	export NODE_ENV=development
	npm start
cleardb:
	node scripts/clear-db
	
