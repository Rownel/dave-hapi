var examplePolicy = (request, response, next) => {
 
  if(request.params.name === 'john' 
    && request.query.age === '10'){
    var data = {
      name: 'John Doe',
      age: 10,
      email: 'john@gmail.com'
    }

    next(null, data);
  }else{
    next(null, false)
  }
 
}

examplePolicy.applyPoint = 'onPreHandler'; // Override 
/**
 * onRequest, onPreAuth, onPostAuth, onPreHandler, onPostHandler, onPreResponse
 */
module.exports = examplePolicy;