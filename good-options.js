const winston = require('winston');
const winstonLoggly = require('winston-loggly-bulk');
const goodWinston = require('hapi-good-winston');

const winstonOption = require('./good-winston-options');

winston.add(winston.transports.Loggly, winstonOption.auth);

const options = {
  ops: {
    interval: 1000
  },
  reporters: {
    myConsoleReporter: [{
      module: 'good-squeeze',
        name: 'Squeeze',
        args: [{
          log: '*',
          response: '*'
        }]
      }, {
        module: 'good-console'
    }, 'stdout'],
    myFileReporter: [{
      module: 'good-squeeze',
      name: 'Squeeze',
      args: [{
        ops: '*'
      }]
    }, {
    module: 'good-squeeze',
      name: 'SafeJson'
    }, {
    module: 'good-file',
      args: ['./test/fixtures/awesome_log']
    }],
    myHTTPReporter: [{
      module: 'good-squeeze',
      name: 'Squeeze',
      args: [{
        error: '*'
      }]
    }, {
    module: 'good-http',
      args: ['http://prod.logs:3000', {
        wreck: {
          headers: {
            'x-api-key': 12345
          }
        }
      }]
		}],
		winston:[{
			module: 'hapi-good-winston',
			name:'goodWinston',
			args:[winston]
		} ]
  }
};

module.exports = options;
