const internals = {};

const versionOne = ` API Version: V1`;
const versionTwo = ` API Version: V2`;
internals.applyRoutes = (server, next) => {
  server.route({
    path: '/v1/name',
    method: 'GET',
    handler: (request, reply) => {
      reply(`Version One Route ${versionOne}`)
    }
  });

  server.route({
    path: '/v2/name',
    method: 'GET',
    handler: (request, reply) => {
      reply(`Version Two Route ${versionTwo}`)
    }
  });

  server.route({
    path: '/name',
    method: 'GET',
    handler: (request, reply) => {
      const version = request.pre.apiVersion;
      if(version === 2){
        reply(`Version route ${versionTwo}`)
      }
      if(version === 1){
        reply(`Version route ${versionOne}`)
      }

      reply.redirect('/')
      
    }
  });

  next();
}

module.exports = (server, options, next) => {
  server.dependency([], internals.applyRoutes);
  next();
}

module.exports.attributes = {
  name: 'apiVersion'
}