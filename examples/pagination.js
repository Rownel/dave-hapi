const Joi = require('joi');
const loremLipsum = require('lorem-ipsum');

internals = {};

const dummyDatabase =  [...Array(30)].map((_,i) => {
  return {
    title: loremLipsum({count:1, units: 'sentences', format: 'plain'}),
    body: loremLipsum({count:3, units: 'paragraphs', format: 'plain'}),
    author: loremLipsum({count: 1, units: 'words', format: 'plain'})
  }
});


const generateHTML = ({title, body, author}) => {
  return `<article><h3>${title}</h3><section>${body}</section><hr><p>Authors by : ${author}</p></article>`
}
internals.applyRoutes = (server, next) => {
  
  server.route({
    method: 'GET',
    path: '/books',
    handler: (request, reply) => {
      let {per_page, page} = Object.assign({
        per_page: 12,
        page: 0 
      }, request.query)

      request.totalCount = dummyDatabase.length;  
      let slicedDummyDatabase = dummyDatabase.slice((per_page * (page - 1)), (per_page * page))
      console.log('First', dummyDatabase.indexOf(slicedDummyDatabase[0])); // check first position 
      console.log('Last', dummyDatabase.indexOf(slicedDummyDatabase[(slicedDummyDatabase.length - 1)])); // check last position 
      const htmlContent = slicedDummyDatabase.map((item, index) => generateHTML(item));
      reply(htmlContent)
    },
    config: {
      validate: {
        query: {
          per_page: Joi.number().integer(),
          page: Joi.number().integer(),
          pagination: Joi.boolean()
        }
      },
      plugins: {
        pagination: {
          enabled: true 
        }
      }
    }
  })

  next();
};



module.exports = (server, options, next) => {
  server.dependency([], internals.applyRoutes);
  next();
}

module.exports.attributes = {
  name: 'davePagination'
}