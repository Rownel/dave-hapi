'use strict';
const internals = {};
internals.applyRoutes = (server, next) => {
  server.route({
    method: 'GET',
    path: '/{name?}',
    config: {
      plugins: {
        policies: [
          'examplePolicy'
        ]
      }
    },
    handler: (request, reply) => {
    
      reply('HELLO WORLD')
    }
  });

  next()
}

exports.register = (server, options, next) =>{
  server.dependency([], internals.applyRoutes)
  next();
}

exports.register.attributes = {
  name: 'examples'
}