const path = require('path');
const handlebar = require('handlebars');


const homeConfig = {};

homeConfig.applyRoutes = (server, next) => {

  server.route({
    path: '/home',
    method: 'GET',
    handler: (request, reply) => {
      const viewData = {
        title: 'Sample ReCaptcha',
        content: 'This is a simple section for somethig',
        list: [...Array(12)].map((_,index) => index)
      }
      reply.view('home',viewData)
    }
  });

  
  server.views({
    engines: {
      html: handlebar
    },
    relativeTo: __dirname,
    path: './',
    layoutPath: './layout'
  })
  next();
};


module.exports = (server, options, next) => {
  server.dependency([], homeConfig.applyRoutes);
  next();
};


module.exports.attributes = {
  name: 'homePage'
};