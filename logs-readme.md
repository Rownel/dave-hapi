## Working with Loggly 

Loggly cloud service use to log application logs. Loggly provide a free 27 day trial, so anyone can use it for testing.

[Loggly](https://www.loggly.com)

### Setup Good, Winston and Loggly 

- [Hapi Good](https://github.com/hapijs/good) use for logs, with the help of [Winston](https://github.com/winstonjs/winston), [Good Winston](https://github.com/alexandrebodin/hapi-good-winston),[Winston Loggly](https://github.com/loggly/winston-loggly-bulk), we can connect loggly and out web application.

- Check `good-options.js` and `good-winston-options` files 
- Please copy `LOGGLY_TOKEN` and `LOGGLY_SUBDOMAIN` to `.env` file 
- Restart Web Server 
- Check Server log on Terminal and Loggly Website 
