'use strict'
const Confidence = require('confidence')
const Config = require('./config')
/* @TODO Move all config/options inside config folder, filename: file-[config|option].js */
const hapiGoodOptions = require('./good-options');
const redis = require('redis')
const redisClient = redis.createClient({host:process.env.REDIS_HOST})

/* Pagination Configuration */
const paginationDefault = require('./config/pagination.config')
const criteria = {
  env: process.env.NODE_ENV
}

const manifest = {
  $meta: 'This file defines the plot device.',
  server: {
    debug: {
      request: ['error']
    },
    connections: {
      routes: {
        security: true
      }
    },
    app: {
      users: {},
      withdrawsActive: {}
    }
  },
  connections: [{
    port: Config.get('/port/web'),
    labels: ['web']
  }],
  registrations: [
    {
      plugin: {
        register: './server/lib/hapi-attempts-limiter',
        options: {
          namespace: process.env.APP_NAME,
          global: {
            limit: 50,
            duration: 5
          }
        }
      }
    },
    {
      plugin: 'hapi-auth-basic'
    },
    {
      plugin: 'lout'
    },
    {
      plugin: 'inert'
    },
    {
      plugin: 'vision'
    },
    {
      plugin: {
        register: 'hapi-io'
      }
    },
    {
      plugin: {
        register: 'therealyou'
      }
    },
    {
      plugin: 'blipp'
    },
    {
      plugin: {
        register: 'hapi-response-time'
      }
    },
    {
      plugin: {
        register: 'visionary',
        options: {
          engines: { jade: 'jade' },
          path: './server/web'
        }
      }
    },
    {
      plugin: {
        register: 'hapi-mongo-models',
        options: {
          mongodb: Config.get('/hapiMongoModels/mongodb'),
          models: {
            Account: './server/models/account',
            AdminGroup: './server/models/admin-group',
            Admin: './server/models/admin',
            AuthAttempt: './server/models/auth-attempt',
            Session: './server/models/session',
            Status: './server/models/status',
            User: './server/models/user',
            Transaction: './server/models/transaction',
            Withdraw: './server/models/withdraw',
            Wallet: './server/models/wallet',
            DepositAddress: './server/models/deposit-address',
            IpLog: './server/models/ip-log'
          },
          autoIndex: Config.get('/hapiMongoModels/autoIndex')
        }
      }
    },
    {
      plugin: './server/auth'
    },
    {
      plugin: './server/mailer'
    },
    {
      plugin: './server/tasks/deposits-bitcoin'
    },
    {
      plugin: {
        register: 'hapi-job-queue',
        options: {
          connectionUrl: process.env.MONGODB_URI,

          jobs: [
            {
              name: 'check-deposits-bitcoin',
              enabled: true,
              cron: '* * * * *',
              concurrentTasks: 1,
              method: 'checkDepositsBitcoin', // server method
              tasks: [ // each task will run with the task as the data property
                {
                  group: 'checkDepositsBitcoin'
                }
              ]
            },
            {
              name: 'test-promises',
              enabled: true,
              schedule: 'every 10 seconds',
              method: 'testPromises',
              tasks: [
                {
                  name: "Sample FAIL Promise",
                  method: 's1'
                }
              ]
            }
          ]
        }
      }
    },
    {
      plugin: {
        register: 'tv',
        options:{
          endpoint: '/tv-logs',
          host: 'localhost',
          port: 8082
        }
      }
    },
    {
      plugin: './server/api/socket',
    },
    {
      plugin: './server/api/transactions',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/wallets',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/qrcode',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/cashier',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/accounts',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/admin-groups',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/admins',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/auth-attempts',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/contact',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/index',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/login',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/logout',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/sessions',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/signup',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/statuses',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/api/users',
      options: {
        routes: { prefix: '/api' }
      }
    },
    {
      plugin: './server/web/index'
		},
		{
			plugin:{
				register: 'good',
      }
    },
    {
      plugin: {
			  register: 'hapi-sanitize-payload',
			  options:{
				  pruneMethod: 'delete' // other option `replace`
			  }
      },
    },
    {
      plugin: {
        register: 'disinfect',
        options: {
          deleteEmpty: true,
          disinfectQuery: true,
          disinfectParams: true,
          disinfectPayload: true
        }
      }
    },
    {
      plugin: {
        register: 'mrhorse',
        options: {
          policyDirectory: __dirname + '/policies',
          defaultApplyPoint: 'onPreHandler'
        }
      }
    },
    {
      plugin: {
        register: 'hapi-api-version',
        options: {
          validVersions: [1, 2, 3],
          defaultVersion: 3,
          vendorName: 'v1'
        }
      }
    },
    {
      plugin: __dirname + '/examples/examples',
      options:{
        routes: {
          prefix: '/examples'
        }
      }
    },
    {
      plugin:{
        register: 'hapi-recaptcha',
        options: {
          sitekey: process.env.RECAPTCHA_SITE_KEY,
        }
      }
    },
    {
      plugin: {
        register: 'hapi-pagination',
        options: paginationDefault
      }
    },
    {
      plugin: `${__dirname}/examples/pagination`
    },
    {
      plugin: __dirname+'/examples/home'
    },
    {
      plugin: __dirname+ '/examples/api-version'
    }
  ]
}

const store = new Confidence.Store(manifest)

exports.get = function (key) {
  return store.get(key, criteria)
}

exports.meta = function (key) {
  return store.meta(key, criteria)
}
